﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ConTester
{

    [XmlType]
    [XmlInclude(typeof(Answer))]
    public class Question
    {
        public Question()
        {
            this.Answers = new List<Answer>();
        }

        [XmlAttribute]
        public string Name { get; set; }
        public string Text { get; set; }
        [XmlArray]
        public List<Answer> Answers { get; set; }

        public void AddAnswer(string text, bool isCorrect = false)
        {
            this.Answers.Add(new Answer(text, isCorrect));
        }

        public int GetCorrectIndex()
        {
            return Answers.IndexOf(GetCorrectAnswer());
        }

        public Answer GetCorrectAnswer()
        {
            return Answers.FindLast(x => x.IsCorrect);
        }

        public string GetFormatted()
        {
            return $"{this.Name}\n{this.Text}\n\n{GetFormattedAnswers()}";
        }


        private string GetFormattedAnswers()
        {
            return $"{GetFormattedAnswer(Answers[0])}\n\n{GetFormattedAnswer(Answers[1])}\n\n{GetFormattedAnswer(Answers[2])}";

        }
        private string GetFormattedAnswer(Answer a)
        {
            return $"{a.Text}";
        }

        public bool IsIndexCorrect(int index)
        {
            if (index > this.Answers.Count) return false;
            return this.Answers[index].IsCorrect;
        }
    }

}
