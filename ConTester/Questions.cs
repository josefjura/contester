﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ConTester
{

    [XmlType]
    [XmlInclude(typeof(Question))]
    public class Questions : List<Question>
    {

    }

}
