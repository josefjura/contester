﻿using ConTester.Properties;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConTester
{
    partial class Program
    {
        static void Main(string[] args)
        {
            ConTester ct = new ConTester();
            Console.WriteLine("ZP ConTester 1.0");
            ct.Init(Settings.Default.XmlPath);
            Console.WriteLine($"Init completed. Using XML from: '{ct.XmlPath}'");


            var input = "status";

            Console.WriteLine("Press enter to start...");
            Console.ReadLine();

            while (input != "quit")
            {
                Console.Clear();

                DrawStats(ct);
                ct.NextQuestion();

                Console.WriteLine(ct.CurrentQuestion.GetFormatted());
                Console.WriteLine();

                Console.Write($"({ct.Finished:D3}/{ct.Total}):");

                if (ct.CurrentAnswer != null)
                {
                    DrawResults(ct, input);
                }

                input = Console.ReadLine();
                var index = RepairInput(input);
                ct.AnswerQuestion(index);
            }
        }

        private static void DrawStats(ConTester ct)
        {

        }

        private static void DrawResults(ConTester ct, string input)
        {
            var left = Console.CursorLeft;
            var top = Console.CursorTop;
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("                  PREVIOUS                     ");
            Console.WriteLine("-----------------------------------------------");

            var correct = ct.CurrentAnswer.Question.GetCorrectAnswer();
            Console.WriteLine(ct.CurrentAnswer.Question.Text);

            if (!string.IsNullOrWhiteSpace(input))
            {
                if (ct.CurrentAnswer.Success)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Answer '{input}' is correct");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Answer '{input}' is not correct:");
                    Console.WriteLine(ct.CurrentAnswer.CorrectAnswer);
                    Console.ResetColor();
                    Console.WriteLine($"Correct answer is :");
                    Console.WriteLine(correct.Text);
                }
            }
            else
            {
                Console.WriteLine("Correct answer was:");
                Console.WriteLine(correct.Text);
            }

            Console.SetCursorPosition(left, top);
        }

        private static int RepairInput(string input)
        {
            if ("123".Contains(input))
            {
                return int.Parse(input) - 1;
            }
            else if ("ABC".Contains(input.ToUpper()))
            {
                return input.ToUpper().First() - 65;
            }
            else
            {
                return 1;
            }
        }
    }
}
