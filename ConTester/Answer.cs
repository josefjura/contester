﻿using System.Xml.Serialization;

namespace ConTester
{

        [XmlType]
        public class Answer
        {
            public Answer()
            {

            }

            public Answer(string text, bool isCorrect)
            {
                this.Text = text;
                this.IsCorrect = isCorrect;
            }

            [XmlAttribute]
            public bool IsCorrect { get; set; }
            public string Text { get; set; }
        }
    
}
