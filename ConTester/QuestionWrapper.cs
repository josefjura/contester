﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConTester
{
    public class QuestionWrapper
    {
        public Question Content { get; set; }
        public int Tried { get; internal set; }
        public int Correct { get; internal set; }
        public int Wrong { get; internal set; }

        public QuestionWrapper(Question q)
        {
            this.Content = q;
        }
    }
}
