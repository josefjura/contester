﻿namespace ConTester
{
    public class AnswerResult
    {
        public bool Success { get; internal set; }
        public Question Question { get; internal set; }
        public string CorrectAnswer { get; internal set; }
    }
}