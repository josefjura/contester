﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConTester
{
    public class TestRun
    {
        public TestRun(List<QuestionWrapper> questions)
        {
            this.AvailableQuestions = new List<QuestionWrapper>(questions);
        }
        List<QuestionWrapper> AvailableQuestions { get; set; }

        public int Remaining => AvailableQuestions.Count;
        public int Finished { get; private set; }

        internal QuestionWrapper PopQuestion()
        {
            var rnd = new Random().Next(AvailableQuestions.Count() - 1);
            var q = AvailableQuestions.PopAt(rnd);
            CurrentQuestion = q;
            Finished++;
            return q;
        }

        public QuestionWrapper CurrentQuestion { get; private set; }
    }
}
