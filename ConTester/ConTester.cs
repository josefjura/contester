﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using static ConTester.Program;

namespace ConTester
{
    class ConTester
    {
        private string xmlPath;

        public void NextQuestion()
        {
            Run.PopQuestion();
        }

        public List<QuestionWrapper> Data { get; private set; }
        public TestRun Run { get; private set; }

        public void Init(string args)
        {
            if (string.IsNullOrWhiteSpace(args)) { Console.WriteLine("You need to provide a path to XML file with input"); }

            if (!File.Exists(args)) { Console.WriteLine("Provided file does not exist"); }

            this.xmlPath = args;

            var qs = ReadXmlData(xmlPath);

            Data = new List<QuestionWrapper>(qs.Select(x => new QuestionWrapper(x)));

            Run = new TestRun(Data);
        }

        private Questions ReadXmlData(string xmlPath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Questions));
            using (FileStream fileStream = new FileStream(xmlPath, FileMode.Open))
            {
                return (Questions)serializer.Deserialize(fileStream);
            }
        }

        public void AnswerQuestion(int answer)
        {
            var result = new AnswerResult();

            var corrIndex = CurrentQuestion.GetCorrectIndex();


            result.Question = CurrentQuestion;
            result.CorrectAnswer = CurrentQuestion.Answers[answer].Text;
            if (answer == corrIndex)
            {
                result.Success = true;
            }
            else
            {
                result.Success = false;
            }

            CurrentAnswer = result;
        }

        public void Reset()
        {
            Run = new TestRun(Data);
        }

        public int Finished => Run.Finished;

        public int Remaining => Run.Remaining;

        public int Total => Data.Count();

        public string XmlPath => xmlPath;

        public Question CurrentQuestion => Run.CurrentQuestion.Content;
        public AnswerResult CurrentAnswer { get; private set; }
    }
}
